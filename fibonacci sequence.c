#include <stdio.h>

int fibonacciSeq(int n);

int fibonacciSeq(int n)
{
    if (n==0)
        return 0;
    else
        if (n==1)
            return 1;
        else
            return fibonacciSeq(n-1)+fibonacciSeq(n-2);
}


int main()
{
    int  n , i , result;
    printf("Enter the limit: ");
    scanf("%d", &n);
    for(i=0 ; i<=n ; i++)
    {
       result = fibonacciSeq(i);
       printf("%d \n" , result);
    }



}
